package com.technoelevate.Task.Scheduler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SchedulingTask {

	// add logger and date formatter

	private static final Logger logger = LoggerFactory.getLogger(SchedulingTask.class);
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	// this method will execute every 2 seconds
	// @scheduled method do not have any arguments and return type
//	@Scheduled(fixedRate = 2000)
//	public void schedulingTaskWithFixedRate() {
//		logger.info("fixed rate task :: Execution Time -{}", dateTimeFormatter.format(LocalDateTime.now()));
//
//	}

//	@Scheduled(fixedDelay = 2000)
//	public void schedulingTaskWithFixedDelay() {
//
//		logger.info("fixed delay task :: Execution Time -{}", dateTimeFormatter.format(LocalDateTime.now()));
//		try {
//			// 5 seconds delay
//			// total =2+5 = 7 sec delay
//			TimeUnit.SECONDS.sleep(5);
//		} catch (InterruptedException e) {
//			logger.error("ran into an error{}", e);
//			//throw new IllegalStateException(e);
//		}
//	}

	// initial delay 5 seconds and there after it will execute in 2 seconds
//	@Scheduled(initialDelay = 5000, fixedRate = 2000)
//	public void schedulingTaskWithInitialDelay()  {
//		//logger.info("fixed rate task with initial delay :: Execution Time -{}",
//				//dateTimeFormatter.format(LocalDateTime.now()));
//		logger.info("schedulingTaskWithInitialDelay "+new Date());
//	}

	// scheduled task will be executed every minute
	@Scheduled(cron = "0 * * * * ?")
	public void schedulingTaskWithCronExpression() {
		logger.info("cron task :: Execution Time -{}", dateTimeFormatter.format(LocalDateTime.now()));
	}

}
